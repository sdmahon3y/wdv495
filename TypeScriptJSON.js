// JavaScript Document
var student = {
    student_id: 332443,
    student_gpa: 3.6,
    student_courses: ["WDV101", "WDV131", "WDV105"]
};
function readyNow() {
    var studentID = document.getElementById('ID');
    studentID.value = student.student_id.toString(6);
    var studentGPA = document.getElementById('GPA');
    studentGPA.value = student.student_gpa.toString(3);
    for (var x = 0; student.student_courses.length; x++) {
        var course = student.student_courses[x]; //Creates variable for current state
        var list = "<li>" + course + "</li>";
        var courses = courses.concat(list);
    }
    var studentCourses = document.getElementById('COURSES');
    studentCourses.value = courses;
}

readyNow();