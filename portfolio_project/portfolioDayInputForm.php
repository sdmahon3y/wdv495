<?php

session_start();

include 'FormValidation.php';	//access the class file

$validateTool = new FormValidation();	//instantiate a new object

$inFirstName = "";
$inLastName = "";
$inUser = "";
$inPassword = "";
$inProgram = "";
$inEmphasis = "";
$inImage = "";
$inPortfolio = "";
$inLinkedin = "";
$inSecondary = "";
$inHometown = "";
$inState = "";
$inGoals = "";
$inHobbies = "";

$errorFirstName = "";
$errorLastName = "";
$errorUser = "";
$errorPassword = "";
$errorProgram = "";
$errorEmphasis = "";
$errorImage = "";
$errorPortfolio = "";
$errorLinkedin = "";
$errorSecondary = "";
$errorHometown = "";
$errorState = "";
$errorGoals = "";
$errorHobbies = "";

$checkForm = "";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(empty($_POST["testForm"])) {
        
        $inFirstName= $_POST['fName'];
        $inLastName = $_POST['lName'];
        $inUser = $_POST['username'];
        $inPassword = $_POST['password'];
        $inProgram = $_POST['program'];
        $inEmphasis = $_POST['emphasis'];
        $inImage = $_POST['image'];
        $inPortfolio = $_POST['portfolio'];
        $inLinkedin = $_POST['linkedin'];
        $inSecondary = $_POST['secondary'];
        $inHometown = $_POST['hometown'];
        $inState = $_POST['state'];
        $inGoals = $_POST['goals'];
        $inHobbies = $_POST['hobbies'];
        
        if($validateTool->validateRequiredField($inFirstName) == false) {
            $errorFirstName = "Please enter first name.";
        }
        else {
            if($validateTool->validateSpecialCharacter($inFirstName) == true) {
                $errorFirstName = "Please remove any special characters.";
            }
            else {
                if($validateTool->validateLength($inFirstName) == false) {
                $errorFirstName = "Please use less than 255 characters.";
                }
                else {
                $errorFirstName = "";
                }
            }
        }
        
        if($validateTool->validateRequiredField($inLastName) == false) {
            $errorLastName = "Please enter last name.";
        }
        else {
            if($validateTool->validateSpecialCharacter($inLastName) == true) {
                $errorLastName = "Please remove any special characters.";
            }
            else {
                if($validateTool->validateLength($inLastName) == false) {
                $errorLastName = "Please use less than 255 characters.";
                }
                else {
                $errorLastName = "";
                }
            }
        }
        
        if($validateTool->validateRequiredField($inUser) == false) {
            $errorUser = "Please enter your dmacc email address.";
        }
        else {
                
            if($validateTool->validateEmail($inUser) == false) {
                $errorUser = "Please enter a proper email address.";
            }
            else {
                $errorUser = "";
            }
        }
            
        
        if($validateTool->validateRequiredField($inPassword) == false) {
            $errorPassword = "Please enter a password.";
        }
        else {
            if($validateTool->validatePassword($inLastName) == false) {
            $errorPassword = "Password must be more than 6 characters.";
            }
            else {
            $errorPassword = "";
            }
        }
              
        if($validateTool->validateLength($inEmphasis) == false) {
            $errorEmphasis = "Please use less than 255 characters.";
        }
        else {
            $errorEmphasis = "";
        }
        
        if($validateTool->validateRequiredField($inImage) == false) {
            $errorImage = "Please enter image name.";
        }
        else {
            if($validateTool->validateLength($inImage) == false) {
                $errorImage = "Please use less than 255 characters.";
            }
            else {
                $errorImage = "";
            }
        }
        
        if($validateTool->validateLength($inPortfolio) == false) {
            $errorPortfolio = "Please use less than 255 characters.";
        }
        else {
            $errorPortfolio = "";
        }
        
        if($validateTool->validateLength($inLinkedin) == false) {
            $errorLinkedin = "Please use less than 255 characters.";
        }
        else {
            $errorLinkedin = "";
        }
        
        if($validateTool->validateLength($inSecondary) == false) {
            $errorSecondary = "Please use less than 255 characters.";
        }
        else {
            $errorSecondary = "";
        }
        
        if($validateTool->validateLength($inHometown) == false) {
            $errorHometown = "Please use less than 255 characters.";
        }
        else {
            $errorHometown = "";
        }
        

        if($validateTool->validateLength($inGoals) == false) {
            $errorGoals = "Please use less than 255 characters.";
        }
        else {
            $errorGoals = "";
        }
        
        if($validateTool->validateLength($inHobbies) == false) {
            $errorHobbies = "Please use less than 255 characters.";
        }
        else {
            $errorHobbies = "";
        }
        
        
        $checkForm = $errorFirstName . $errorLastName . $errorUser . $errorPassword . $errorProgram . $errorEmphasis . $errorImage . $errorPortfolio . $errorLinkedin . $errorSecondary . $errorHometown . $errorState . $errorGoals . $errorHobbies;
        
        if($checkForm == "") {
            require_once("dbConnector.php");
        
            try {
            //SQL command using placeholders
	        $sql = "INSERT INTO student_info_2020 (student_id, student_first_name, student_last_name, student_username, student_password, student_program, student_emphasis, student_image, student_portfolio, student_linkedin, student_secondary, student_hometown, student_state, student_career_goals, student_hobbies)
            VALUES (NULL, :eFName, :eLName, :eUsername, :ePassword, :eProgram, :eEmphasis, :eImage, :ePortfolio, :eLinkedin, :eSecondary, :eHometown, :eState, :eGoals, :eHobbies);";

	        echo $sql;

	        $statement = $conn->prepare($sql);
                
            $statement->bindParam(':eFName', $inFirstName);
            $statement->bindParam(':eLName', $inLastName);
            $statement->bindParam(':eUsername', $inUser);
            $statement->bindParam(':ePassword', $inPassword);
            $statement->bindParam(':eProgram', $inProgram);
            $statement->bindParam(':eEmphasis', $inEmphasis);
            $statement->bindParam(':eImage', $inImage);
            $statement->bindParam(':ePortfolio', $inPortfolio);
            $statement->bindParam(':eLinkedin', $inLinkedin);
            $statement->bindParam(':eSecondary', $inSecondary);
            $statement->bindParam(':eHometown', $inHometown);
            $statement->bindParam(':eState', $inState);
            $statement->bindParam(':eGoals', $inGoals);
            $statement->bindParam(':eHobbies', $inHobbies);

	        $statement->execute();
	        
	        echo "<script>alert('Submitted');</script>";
            }
            catch(PDOException $e){
                echo "Process failed: " . $e->getMessage();
                }
            }
        else {
            echo "<script>console.log('Please fix any errors in input fields before submitting again.');</script>";
        }
    }
}
        
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Portfolio Day Registration</title>
<style>
    
.error	{
	color:red;
	font-style:italic;	
}
    
#testForm {
    display: none;
}

span {
	color: red;
}
    
</style>
</head>

<body>
    
    <h1>Student Input Form</h1>
    <form name="insert" method="post" action="portfolioDayInputForm.php">
    <p>
        <input type="text" name="test" id="testForm" value="">
        <label for="fName">First Name:</label>
        <input type="text" name="fName" id="fName" value="<?php echo $inFirstName ?>"> <span><?php echo $errorFirstName ?></span>
    </p>
    <p>
        <label for="lName">Last Name:</label>
        <input type="text" name="lName" id="lName" value="<?php echo $inLastName ?>"> <span><?php echo $errorLastName ?></span>
    </p>
    <p>
        <label for="username">Dmacc Email Address(Username):</label>
        <input type="text" name="username" id="username" value="<?php echo $inUser ?>"> <span><?php echo $errorUser ?></span>
    </p>
    <p>
        <label for="password">Password:</label>
        <input type="text" name="password" id="password" value="<?php echo $inPassword ?>"> <span><?php echo $errorPassword ?></span>
    </p>
    <p>
        <label for="program">Program:</label>
        <select name="program" id="program" value="<?php echo $inProgram ?>">
            <option value="animation">Animation</option>
            <option value="graphic_design">Graphic Design</option>
            <option value="photography">Photography</option>
            <option value="video_production">Video Production</option>
            <option value="web_development">Web Development</option>
        </select>
    </p>
    <p>
        <label for="minor">Emphasis:</label>
        <input type="text" name="emphasis" id="emphasis" value="<?php echo $inEmphasis ?>"> <span><?php echo $errorEmphasis ?></span>
    </p>
    <p>
        <label for="image">Student Image:</label>
        <input type="text" name="image" id="image" value="<?php echo $inImage ?>"> <span><?php echo $errorImage ?></span>
    </p>
    <h2>Web Links</h2>
    <div>
        <p>
            <label for="portfolio">Portfolio Page:</label>
            <input type="text" name="portfolio" id="portfolio" value="<?php echo $inPortfolio ?>"> <span><?php echo $errorPortfolio ?></span>
        </p>
        <p>
            <label for="linkedin">Linked In:</label>
            <input type="text" name="linkedin" id="linkedin" value="<?php echo $inLinkedin ?>"> <span><?php echo $errorLinkedin ?></span>
        </p>
        <p>
            <label for="secondary">Secondary Website:</label>
            <input type="text" name="secondary" id="secondary" value="<?php echo $inSecondary ?>"> <span><?php echo $errorSecondary ?></span>
        </p>
    </div>
    <h2>Additional Information</h2>
        <p>
            <label for="hometown">Hometown:</label>
            <input type="text" name="hometown" id="hometown" value="<?php echo $inHometown ?>"> <span><?php echo $errorHometown ?></span>
        </p>
        <p>
            <label for="state">State:</label>
            <select name="state" id="state" value="<?php echo $inState ?>">
                <option value="AL">AL</option>
                <option value="AK">AK</option>
                <option value="AS">AS</option>
                <option value="AZ">AZ</option>
                <option value="AR">AR</option>
                <option value="CA">CA</option>
                <option value="CO">CO</option>
                <option value="CT">CT</option>
                <option value="DE">DE</option>
                <option value="DC">DC</option>
                <option value="FM">FM</option>
                <option value="FL">FL</option>
                <option value="GA">GA</option>
                <option value="GU">GU</option>
                <option value="HI">HI</option>
                <option value="ID">ID</option>
                <option value="IL">IL</option>
                <option value="IN">IN</option>
                <option value="IA">IA</option>
                <option value="KS">KS</option>
                <option value="KY">KY</option>
                <option value="LA">LA</option>
                <option value="ME">ME</option>
                <option value="MH">MH</option>
                <option value="MD">MD</option>
                <option value="MA">MA</option>
                <option value="MI">MI</option>
                <option value="MN">MN</option>
                <option value="MS">MS</option>
                <option value="MO">MO</option>
                <option value="MT">MT</option>
                <option value="NE">NE</option>
                <option value="NV">NV</option>
                <option value="NH">NH</option>
                <option value="NJ">NJ</option>
                <option value="NM">NM</option>
                <option value="NY">NY</option>
                <option value="NC">NC</option>
                <option value="ND">ND</option>
                <option value="MP">MP</option>
                <option value="OH">OH</option>
                <option value="OK">OK</option>
                <option value="OR">OR</option>
                <option value="PW">PW</option>
                <option value="PA">PA</option>
                <option value="PR">PR</option>
                <option value="RI">RI</option>
                <option value="SC">SC</option>
                <option value="SD">SD</option>
                <option value="TN">TN</option>
                <option value="TX">TX</option>
                <option value="UT">UT</option>
                <option value="VT">VT</option>
                <option value="VI">VI</option>
                <option value="VA">VA</option>
                <option value="WA">WA</option>
                <option value="WV">WV</option>
                <option value="WI">WI</option>
                <option value="WY">WY</option>
            </select><span><?php echo $errorState ?></span>
        </p>
        <p>
          <label for="goals">Career Goals:</label>
            <input type="text" name="goals" id="goals" value="<?php echo $inGoals ?>"> <span><?php echo $errorGoals ?></span>
      </p>
        <p>
            <label for="hobbies">Hobbies:</label>
            <input type="text" name="hobbies" id="hobbies" value="<?php echo $inHobbies ?>"> <span><?php echo $errorHobbies ?></span>
        </p>
        <p>
        <input type="submit" name="button1" id="button1" value="Submit">
        <input type="button" name="button2" id="button2" value="Reset">
        </p>
    </form>

</body>
</html>