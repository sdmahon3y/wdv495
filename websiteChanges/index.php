<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link href ="portfolio_site.css" rel = "stylesheet" type = "text/css" />
<link href ="media.css" rel = "stylesheet" type = "text/css" />
<title>Sean Mahoney's Portfolio Site</title>
<script>
    
    function projectLink() {
        location.href = "projects.html";
    }
    
    function contactLink() {
        location.href = "#";
        location.href = "#contactForm";
    }
    
</script>
</head>

<body class="background">
    <div class="container">
        <div class="header">
            <h1>Sean Mahoney</h1> 
            <nav class="nav">
                <h2>Resume</h2>
                <h2 onClick="projectLink()">All Projects</h2>
                <h2 onClick="contactLink()">Contact Page</h2>
            </nav>
        </div>
        <div class="banner">
            <img src="portfolio_site_banner.jpg" alt="Banner from Pixabay">
        </div>
        <div class="flex-container">
            <div class="about">
            <img src="Sean Mahoney.jpg" alt="Portrait">
                <p>Completing AAS Degree in Web Development in Spring of 2020.</p>
                <p>Enjoy working with Javascript, JSON Objects and PHP Forms.</p>
                <p>Raised in Iowa.</p>
            </div>
            <div class="items">
                <h3><a href="wdv205_finished/Assignment_6/float_layout.htm">CSS Float Layout</a></h3>
                <h3><a href="wdv205_finished/Assignment_6/flexbox_layout.htm">CSS Flexbox Layout</a></h3>
                <h3><a href="wdv205_finished/Assignment_6/grid_layout.htm">CSS Grid Layout</a></h3>
                <h3><a href="wdv221_finished/exam_1/labExam.html">Javascript Date Functions</a></h3>
                <h3><a href="wdv221_finished/skill_tests/skill_test_1.html">Javascript Validation &amp; Form Clear</a></h3>
                <h3><a href="wdv221_finished/unit_9/salesTaxLookup.html">Javascript Dynamic Dropdown Array</a></h3>
                <h3><a href="wdv321_finished/recipe/recipeProject.html">Javascript Dynamic Recipe Site</a></h3>
                <h3><a href="wdv321_finished/ajax_project/AJAXTeamProjectForm.html">Javascript AJAX Call Group Project</a></h3>
                <h3><a href="WDV240/project3/">Wordpress Events &amp; Login</a></h3>
                <h3><a href="WDV240/project5/">Wordpress Woocommerce Store</a></h3>
                <h3><a href="WDV240/project6/">Wordpress Events, Store &amp; Login</a></h3>
                <h3><a href="wdv341_finished/forms/customerRegistrationForm.php">PHP Self-Posting Form With Validation</a></h3>
                <h3><a href="wdv341_finished/sql/selectEvents.php">PHP SQL Select</a></h3>
                <h3><a href="wdv341_finished/final_project/index.php">PHP Login, Insert, Update &amp; Delete</a></h3>
                <h3><a href="wdv351_finished/digital/paypalDigital.php">Paypal Checkout: Digital Product</a></h3>
                <h3><a href="wdv495_finished/recipe_pwa/index.html">Recipe PWA</a></h3>
                <h3><a href="wdv495_finished/portfolio_project/portfolioDaySplash.php">Portfolio Day Splash Draft</a></h3>
            </div>
        </div>
    <div class="contactForm" id="contactForm">
        <form name="contact" method="post" action="contact.php">
            <h2>Send me a Message</h2>
            <div class="contactFlex">
            <div>
            <p>
                <label for="firstName">First Name:</label>
                <input type="text" name="firstName" id="firstName" value="">
            </p>
            <p>
                <label for="lastName">Last Name:</label>
                <input type="text" name="lastName" id="lastName" value="">
            </p>
            <p>
                <label for="email">Email Address:</label>
                <input type="text" name="email" id="email" value="">
            </p>
            </div>
            <div>
                <p>
                <label for="message">Message:</label>
                </p>
                <textarea name="message" id="message" value=""></textarea>
            </div>
            </div>
            <p>
                <input type="submit" name="submit" id="submit" value="Submit">
                <input type="button" name="clear" id="clear" value="Reset">
            </p>
            </div>
        </form>
    </div>
    </div>
</body>
</html>