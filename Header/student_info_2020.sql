-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 10.123.0.64:3306
-- Generation Time: Apr 28, 2020 at 09:48 PM
-- Server version: 5.7.24
-- PHP Version: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gullydsm_dmacc`
--

-- --------------------------------------------------------

--
-- Table structure for table `student_info_2020`
--

CREATE TABLE `student_info_2020` (
  `student_id` int(11) NOT NULL,
  `student_email` varchar(100) NOT NULL,
  `student_password` char(60) NOT NULL,
  `student_first_name` varchar(100) DEFAULT NULL,
  `student_last_name` varchar(100) DEFAULT NULL,
  `student_program` varchar(100) DEFAULT NULL,
  `student_portfolio` varchar(255) DEFAULT NULL,
  `student_linkedin` varchar(255) DEFAULT NULL,
  `student_secondary` varchar(255) DEFAULT NULL,
  `student_hometown` varchar(100) DEFAULT NULL,
  `student_career_goals` varchar(255) DEFAULT NULL,
  `student_hobbies` varchar(255) DEFAULT NULL,
  `student_state` char(2) DEFAULT NULL,
  `student_emphasis` varchar(100) DEFAULT NULL,
  `student_public_email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 TABLESPACE `gullydsm_dmacc`;

--
-- Dumping data for table `student_info_2020`
--

INSERT INTO `student_info_2020` (`student_id`, `student_email`, `student_password`, `student_first_name`, `student_last_name`, `student_program`, `student_portfolio`, `student_linkedin`, `student_secondary`, `student_hometown`, `student_career_goals`, `student_hobbies`, `student_state`, `student_emphasis`, `student_public_email`) VALUES
(12, 'eehallahan@dmacc.edu', '$2y$10$gsZj9GWxGXYQL5q0Ka38bOKdXsZCOaSXWJsiiowbGI2YbEMKO46ia', 'Enda', 'Hallahan', 'web-dev', 'https://argabarga.org/', 'https://www.linkedin.com/in/enda-hallahan', 'https://github.com/EndaHallahan', 'West Des Moines', NULL, NULL, 'IA', NULL, NULL),
(13, 'dkwolford@dmacc.edu', '$2y$10$Sqsj7qFj/h6HlHBW1Alb6Oq8b/NFH4he119YHagCkyiyH6l0qRCMq', 'Dalton', 'Wolford', 'web-dev', 'http://daltonwolford.com/portfolio', 'https://www.linkedin.com/in/dalton-w-a2393babsdaffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdddddddddddddddffd/', 'http://daltonwolford.com', 'Waukee', 'I am looking for a career in web development.', NULL, 'IA', NULL, 'wolforddalton@gmail.com'),
(14, 'sdmahoney@dmacc.edu', '$2y$10$Rn7am7UXok1a9d9cAZQO7uD.IB9F77pct6UEeMYmHxgcc8M70KOeO', 'Sean', 'Mahoney', 'web-dev', 'http://sdmahoney.com', NULL, NULL, 'Ankeny', 'Working in a group setting with learning opportunities.', NULL, 'IA', NULL, 'sdmahon3y@gmail.com'),
(15, 'vascholten@dmacc.edu', '$2y$10$F3y.dUihgazN/fmjkNGqM.cZX1HuvMx/xKBo9CboLLzFjmSBqeZDa', 'Victoria', 'Scholten', 'web-dev', 'http://victoriannescholten.com', 'https://www.linkedin.com/in/victoria-scholten-9ba2761a0/', 'https://github.com/victorianne', 'Urbandale', NULL, 'Photography, Painting, Ultimate Frisbee, DIY Projects, Reading, Hammocking and anything outdoors.', 'IA', 'Digital Marketing', 'vascholten@gmail.com'),
(16, 'bowens2@dmacc.edu', '$2y$10$G/EJpas3zuxVDCGrJ6NZge3grtMEA3zAZhgtZqFUl9Xl/qlM6AdOG', 'Bradley', 'Owens', 'web-dev', 'https://www.brvdleyowens.com', 'https://www.linkedin.com/in/brvdleyowens/', 'https://github.com/brvdley', 'Chariton', 'My goals center around working in a web development workshop or working as a back-end engineer for any company interested.', 'I enjoy video games, 3D animation, coding, skateboarding, listening to music, and hanging out with friends.', 'IA', NULL, 'brvdleyowens@gmail.com'),
(17, 'drrichards2@dmacc.edu', '$2y$10$ioxeN15nVbsO11p1oyPNeO5sYm55zwxyhD9iyokCGdnb82XWcbXDa', 'Daniel', 'Richards', 'video-production', 'https://www.richardsvideoproduction.com', 'https://www.linkedin.com/in/drrichards1989', NULL, 'Ankeny', 'Find my place in the Christian film and entertainment industry and have a positive impact on as many people as possible while making money and having fun along the way.', 'camping, kayaking, hiking, reading, and family/friends time.', 'IA', 'Adobe Premier / After Effects', 'DanielRaymondRichards@gmail.com'),
(19, 'jmschilling@dmacc.edu', '$2y$10$PjdYj7YVaLnViFkC5SL1WutwR1.dVF/HidicAE57O/.6CSai1IxS.', 'Jack', 'Schilling', 'video-production', 'https://schillingmedia.wixsite.com/schillingreel', 'https://www.linkedin.com/in/jack-schilling-1a7642169/', NULL, 'Jefferson', 'Utilizing video production and multimedia endeavors to the best of their abilities while building business.', 'Video games, acting, singing, and playing musical instruments.', 'IA', 'Entrepreneurship', 'jackmschilling@gmail.com'),
(21, 'tmdo2@dmacc.edu', '$2y$10$bLRn7tCyylr5wfsgrF5SrOv2ICwhwxHV7y0rQvjPSSOrLTf4e7knK', 'Kristine', 'Do', 'graphic-design', 'https://kristine.squarespace.com/', 'https://www.linkedin.com/in/kristine-do-442611166', 'https://www.behance.net/thuydo1', 'Des Moines', 'work in professional places', 'Drawing while listening K-pop with my cats lay down around me. ', 'IA', NULL, 'kristine.mtdo@gmail.com'),
(22, 'vasquier@dmacc.edu', '$2y$10$rmg2Vikn1FsDRQDDQPCDlOkl8MlW65zS.AmEMVqwsthOeN5MUx7uu', 'Vanessa', 'Squier', 'graphic-design', 'http://vasquier.com/', 'https://www.linkedin.com/in/vanessa-squier', NULL, 'Rockwell City', 'My true personal goal lies within helping non-profits make a difference in lives. Having experienced\r\nthe impact hospitals can provide to families, I strive to find ways to give back to those who have\r\ngiven so much to my family and friends.', 'I have many hobbies, including knitting, painting, and anything sports-related. My biggest hobby, however, is racing.', 'IA', 'Photography', 'vasquier@gmail.com'),
(23, 'segoche@dmacc.edu', '$2y$10$Io87xwMi2oVdqVJYhjCxzuAbNPkOwJSefQBGe.q7U21w01vMphoYa', 'Shayla', 'Goche', 'graphic-design', 'https://shaylagochedesign.net', 'https://www.linkedin.com/in/shayla-goche', NULL, 'Algona', 'My goal is to work as a freelance graphic designer for now, as I continue to work as a makeup artist in the Ankeny area. ', 'I enjoy exercising, illustrating, traveling, working as a makeup artist, and spending free time with family and friends. ', 'IA', NULL, 'shayla.goche17@gmail.com'),
(24, 'tkdunkin@dmacc.edu', '$2y$10$zLBzZkDJFzw4rznSR582I.cp1yMqhB4jZkamETkm.YMDxlClf6Xh6', 'Tanner', 'Dunkin', 'graphic-design', 'http://tkdunkin.com', NULL, NULL, 'Knoxville', NULL, 'Traveling\r\nSports\r\nCraft Beer', 'IA', NULL, 'tdunkin6@gmail.com'),
(25, 'mcoleman10@dmacc.edu', '$2y$10$NICFtVo4MC5wWhbJawlNde.02JLInyZcAQvybJh1iCc2aoxW1/BQ2', 'Mary', 'Coleman', 'animation', NULL, NULL, NULL, 'Ames', '3D Animator and Concept Artist\r\nI hope to one day work in the TV/Film and Gaming industries using these skills.', 'Creative writing, gaming, drawing', 'IA', NULL, 'marybeth.coleman1998@gmail.com'),
(26, 'csmith47@dmacc.edu', '$2y$10$ar8qh1/jgemi3QPhI37UBOSF9v0668fiuUxkw4FzQZOeuv8.aju/O', 'Carl', 'Smith', 'animation', NULL, NULL, NULL, 'Elkhart', '3D modeling and animation', NULL, 'IA', NULL, 'carlsmithanimation@gmail.com'),
(27, 'idfolger@dmacc.edu', '$2y$10$aayJDJ7Ya4iNeqjvja7Eee1AFByVJT2KsOGAY50qH03soFlUfvC5O', 'Ian', 'Folger', 'animation', 'https://vimeo.com/412408576', NULL, NULL, 'Ames', 'Getting a job in game animation.', 'I do enjoy gaming. This helped me decide on what I wanted to do for a job. Work in the creation of a media that I enjoyed myself.', 'IA', NULL, 'folgerian.animation@gmail.com'),
(28, 'tthomsen@dmacc.edu', '$2y$10$IuwwYWk5MPlz9IPeNtcMoeubc691ALBMBKCZ0eHHNGtyuyDkZTz6m', 'Jay', 'Thomsen', 'animation', 'https://vimeo.com/user109033728', NULL, NULL, 'Des Moines', 'Game Animation, Modeler, And Character Design', 'Sewing, sculpting, drawing, painting, redesigning/customizing dolls, and costume creation', 'IA', NULL, 'trinity.thomsen@gmail.com'),
(29, 'zrmiller1@dmacc.edu', '$2y$10$mga5cadYBLD5W6tAcf0rrOmmhuaUhAxs3.5tin0pfPs5pqY.27.L2', 'Zachary', 'Miller', 'animation', NULL, NULL, NULL, 'Des Moines', 'To get a job building 3D assets for the video game industry, ranging from props, to vehicles, and even level and environment design, as well as character design and animation.', 'Role-playing games, to take the assets given to me and flesh out unique and life-like characters', 'IA', '3D Art and Animation', 'zmillerworks04@gmail.com'),
(30, 'ajpogwizd@dmacc.edu', '$2y$10$GC/Kxyr21KPI4XRVm9RO.uovlYGHuE0nhIPjLRICVV6TVirCZGed2', 'Austin', 'Pogwizd', 'animation', 'https://vimeo.com/412381104', NULL, NULL, 'Nevada', '3D Generalist and Animator, with possibly some 2D on the side', 'Art, trading cards, video games, and animated shows, especially anime', 'IA', NULL, 'APogwizdAnimates@gmail.com'),
(31, 'bvansickle1@dmacc.edu', '$2y$10$YCKgnHmCEbE6UIWl6/pm..RD4QJUZhx.5Vfsn73yiTqxGy8x61t4a', 'Bauston', 'Van Sickle', 'animation', NULL, NULL, NULL, 'Newton', 'Animator for entertainment.', NULL, 'IA', NULL, 'vbauston@gmail.com'),
(32, 'pgreene@dmacc.edu', '$2y$10$leUiqj98S7Mgs1g3x.cG0eQLYJQ/z5VgNx4kWSa1Q28WnmKYHsDx.', 'Purmon', 'Greene', 'animation', NULL, NULL, NULL, 'Elkheart', 'my career goals are to work at a major studio, then create my own studio and retire as a teacher.', 'playing Piano and drawing.', 'IA', 'Animator', 'purmongx@gmail.com'),
(33, 'shjones@dmacc.edu', '$2y$10$wDiIoA3Jp7EMf5X0xB6Jb.VnHphGWCcgiHg4viNZRYljsYwBslnS2', 'Sam', 'Jones', 'animation', NULL, NULL, NULL, NULL, 'Get an internship.', NULL, 'IA', 'Graphic Design', 'shjones@dmacc.edu'),
(35, 'jcasey3@dmacc.edu', '$2y$10$DwD1pE0r2H2BVJMaG26The04vVc3TZ9GxcpLPFow4O2DKrETvbaym', 'Jayden', 'Casey', 'animation', 'https://vimeo.com/412200593', NULL, NULL, 'Ankeny', '3D modeling', 'Art\r\nWriting', 'IA', NULL, 'bluebirdart99@gmail.com'),
(36, 'aavanessen@dmacc.edu', '$2y$10$MLfw3PhkFKSwwTMqE5NHEe3M1H.XOsfrWuHQBG2MhQhgZ9cMbhrl2', 'Sasha', 'Van Essen', 'animation', 'https://vimeo.com/412414571', NULL, NULL, 'Norwalk', 'After Dmacc I hope to get a job for Nintendo 3D animation and continue to make content for my personal business. ', 'My hobbies are drawing, play video games, hike, and working out/run. ', 'IA', NULL, 'sashavanessen@gmail.com'),
(37, 'cacarpenter5@dmacc.edu', '$2y$10$B2D5q2CsTd7DdNF63aYdveKHTKTBN5YTPAp6O0syTKob0GE9m0lzi', 'Chloe', 'Carpenter', 'graphic-design', 'https://www.chloecarpenterdesigns.com', 'https://www.linkedin.com/in/carpenterchloe/', NULL, 'Grimes', 'My main career goal is to find a job that I look forward to going to every day, a job where I can be creative and have fun while doing my work. I would love to work at a place where I can collaborate with others and continue learning new skills every day.', NULL, 'IA', NULL, 'Chloe.Carpenter8@gmail.com'),
(38, 'karobinson5@dmacc.edu', '$2y$10$2F.qj0JOoqCZnRUR66758OklcfsicJaRuiwS39c3xPLvSGSJYZzKy', 'Kassie', 'Robinson', 'graphic-design', 'http://kassierobinson.com/', 'https://www.linkedin.com/', NULL, 'Colo', 'I strive to succeed in my Graphic Design career. My goals include learning the most that I can to improve my design skills every day and to bring successful Visual Communication through design to all of my work. ', 'I enjoy playing sports, cooking, painting and drawing, shopping, hanging out with my family and watching movies.  ', 'IA', NULL, 'kassierob13@gmail.com'),
(39, 'slrounceville@dmacc.edu', '$2y$10$tOJS/Xur9S3J5wWW4d21geZtiWXsynTt7vX21kpN6LkI8F1sIAhTi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'kneese1@dmacc.edu', '$2y$10$ev6gBc.o4FCn4vi1C.w6KeCWEaoP92Wg1Mgmy9sOhTbAYKD0iF0LG', 'Kathryn', 'Neese', 'graphic-design', 'https://kneese1.myportfolio.com', 'https://www.linkedin.com/in/kathryn-neese-8b137918b/', NULL, 'West Des Moines', 'I aspire to be a graphic designer in the Des Moines area. Currently I am applying for internships and entry-level jobs, so I can gain more skills and experience to be a well-rounded graphic designer. ', 'During my free time, I love to paint, draw, bake, get outdoors, and try new things. I also love to spend time with my cats.', 'IA', NULL, 'katneese2017@gmail.com'),
(41, 'mjmillburn@dmacc.edu', '$2y$10$RS1TlqcLu2b4PdI/c3Y27OvFK4mftnQKlibO4TZxQvTXn8JXS4USq', 'Mattea ', 'Millburn', 'graphic-design', 'http://matteamillburn.com/portfolio/', 'https://www.linkedin.com/in/mattea-millburn-ab737118b/', NULL, 'Des Moines', 'To make use of my creative skills at a forward-looking company that encourages individual growth and focuses on finding innovative solutions for their clientele.', 'When I\'m not studying graphic design I enjoy playing video games, listening to music, watching animated movies, drawing and working on my next cosplay.  ', 'IA', NULL, NULL),
(42, 'ggamboa@dmacc.edu', '$2y$10$lJM9xM3zy/oONZZK2pq6r.QeC0q9/3JtkFvICf1PiPd6dwDs8kT4q', 'Guadalupe', 'Gamboa', 'graphic-design', 'http://lupe-gamboa.com/', NULL, NULL, 'Grimes', NULL, 'Drawing, playing video games, and making crafts.', 'IA', NULL, 'echokiwiii@gmail.com'),
(44, 'iecabezas@dmacc.edu', '$2y$10$Qq4GjrDhhDUKwY.ijEyfneIe0GRWZMLw5oCOJYygGgqXtgeEiZ48G', 'Ingri', 'Cabezas', 'graphic-design', 'http://ingricabezas.com', 'https://www.linkedin.com/in/ingri-cabezas/', NULL, 'Des Moines', 'I would like to experiences different work environments. I want to start in either a studio or agency. In the future, I would love to dive into apparel and package design. ', 'I love painting any chance I get and being around family and friends. I also create designs to print on t-shirts, hoodies etc. It\'s very rewarding to able to create a product and see others wear your work.   ', 'IA', NULL, 'ingri.cabezas@gmail.com'),
(46, 'mpeterson30@dmacc.edu', '$2y$10$U6gg03AcZl8u2Dk09jRYFOgRSmwkXDcc8gCVsTn/Xl52vr3nFpID2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'rkleese@dmacc.edu', '$2y$10$V24kCyrCGzRpqixF4icEIOQs61grelX/TFZTA2d9CqOdbuIAbf3A.', 'Rylin', 'Kleese', 'graphic-design', 'https://www.rtk-designs.com', 'https://www.linkedin.com/feed/?trk=nav_logo', NULL, 'Des Moines', 'I would like to one day be a designer for a professional sports team. Baseball would be my dream sport but any sport would be amazing. I want to learn everything I can to make my designs as best as they can. ', 'I enjoy being active. I like to play baseball, basketball, golf, and bowling. I also enjoy going to the gym. Video games are also something I like to do.  ', 'IA', NULL, 'rylinkleese@gmail.com'),
(48, 'slangman@dmacc.edu', '$2y$10$bIPRGUX1dVTRO7koA0gb5.QElFvOL7StDMOwMskZUbq5QIvJFtNJK', 'Susan', 'Langman', 'graphic-design', 'http://susanlangmandesigns.com', NULL, NULL, 'Waukee', NULL, NULL, 'IA', NULL, 'susan.e.langman@gmail.com'),
(49, 'ssdahlen@dmacc.edu', '$2y$10$h86gNbO1dSX6mHCOhWurlOz.ZsglH1a3Dqof9Sg5jxoj7mHbmTagu', 'Savannah ', 'Dahlen', 'graphic-design', 'https://www.savannahdahlen.com', 'https://www.linkedin.com/in/savannah-dahlen-0b3246186/', NULL, 'Johnston', 'My goal is to work for a group of people that inspire me and encourage me to never stop learning and push me to be the best that I can be. I want to work for a company that allows for a variety of projects and creative mediums.', 'In my free time I enjoy reading, writing, crafting, and spoiling my 2 cats.', 'IA', 'Web Development, Photography', 'savannahsdahlen@gmail.com'),
(50, 'mtkavan@dmacc.edu', '$2y$10$9/Xp8jvdQq67xvpooP2M4OWRA8xL2vG7JkBnv2Uml2vPQz74smoB6', 'mitchell', 'kaan', 'graphic-design', 'http://mtk-graphics.com', 'https://www.linkedin.com/in/mitchell-kavan', NULL, 'des moines', 'to secure a position in the graphic design field', 'painting, eating, working out, drawing', 'IA', NULL, 'mitchellkavan753@gmail.com'),
(51, 'rlfoss@dmacc.edu', '$2y$10$DX7l7sYD/rDEVs2C3sbKS.7hc9aBMnPfefxUS3NKvcXg.TE8m6W/S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'tcstransky@dmacc.edu', '$2y$10$32ej.E5rvVBcHF.8q28cqe6BcMJ9.eL55AaClHnNISobdMmRGixoC', 'Tessa', 'Stransky', 'graphic-design', 'https://tessastransky.com', 'https://www.linkedin.com/in/tessa-stransky/', NULL, 'Ames', 'I love package, print, UI/UX and branding design. I hope to work for a company that aligns with my visions and values as a graphic designer.', 'Baking, hiking, and writing.', 'IA', NULL, 'stranstc@gmail.com'),
(53, 'lbhlua@dmacc.edu', '$2y$10$1nnVNx5rDQ8VJAo1sLcRbO63FBOPrWs.k2yJrief0GcTqzyfiv7bi', 'Lal', 'Hlua', 'graphic-design', NULL, NULL, NULL, 'DES MOINES', NULL, NULL, 'IA', NULL, 'kevinvala0@gmail.com'),
(54, 'klleroy@dmacc.edu', '$2y$10$TWv3kTOeSOfn3npXz5S4uefhqaX6Zau97MoWswK5NsZJj4I2ogacG', 'Kat', 'LeRoy', 'graphic-design', 'http://katleroy.com', NULL, NULL, 'DesMoines', NULL, NULL, 'IA', 'Web Dev', 'leroy.kathryn@gmail.com'),
(55, 'abenedict@dmacc.edu', '$2y$10$2JFmwv6BgeFhZaFyI5XwZOkbq1plmohG9kVmpuV20VBD8pMUyTR3C', 'Adnan', 'Benedict', 'graphic-design', 'http://adnanbenedict.we.bs', NULL, NULL, 'Pleasant Hill', NULL, 'Designing album art, making music, producing beats, playing guitar, taking pictures', 'IA', NULL, 'adnanbenedict@gmail.com'),
(56, 'tckoopman1@dmacc.edu', '$2y$10$Gyjg4yKjFZo6gW5993LTQOi3jLL2VgZcSP7cIYdw1Q.exkv14WWdi', 'Tristan', 'Koopman', 'graphic-design', 'https://www.koopman-designs.com/', 'https://www.linkedin.com/in/tristan-koopman-9452b6b7/', NULL, 'Jewell', 'In the future my career goals are to work for a studio or agency in the Des Moines area where I hope to make things look good. ', 'Some of my hobbies include hiking, gardening, watching movies, and drawing. \r\n', 'IA', 'Liberal Arts ', 'collier.koop@gmail.com'),
(58, 'jamarquez2@dmacc.edu', '$2y$10$uySm27/DOpWQ0OMbLlWLyOkODwf2Iop67vKX9wqf6lQo7oZZsonT2', 'Jennifer', 'Marquez', 'graphic-design', 'https://jamsdesigns.me', NULL, NULL, 'Des Moines', NULL, 'Photography', 'IA', NULL, 'jamsdesign.me@gmail.com'),
(59, 'hjoconnormorelock@dmacc.edu', '$2y$10$.bSF4gg1Ahx.CubWTdwuvO6f3V8/NrZ39/trUW68psTHnOlauwfMK', 'Hailey', 'O\'Connor-Morelock', 'graphic-design', 'http://haileyocm.com/wordpress/', NULL, NULL, 'Des Moines', 'I would love to get a position where I could work with designing book covers, album covers, event posters, or product design. I love illustration and I also enjoy typography so a career combining those two things would be my goal.', 'My hobbies are drawing, painting, playing video games,  reading and writing poetry, playing guitar, photography, and watching movies/TV.', 'IA', NULL, 'haileyoconnorr@icloud.com'),
(60, 'sajoy@dmacc.edu', '$2y$10$C5AXuDlzH.X3gvzye7T5feQ.dOc4ASaEu5cSxDdX/2I/dXf6gWtbC', 'Stephanie', 'Joy', 'graphic-design', 'https://stephanieajoys.wixsite.com/website', 'https://www.linkedin.com', NULL, 'Jefferson', 'Gaining experience to feel accomplished as a well-rounded designer.', 'Video games, photography, small potted plants', 'IA', 'Advertising', 'stephanie.ajoys@gmail.com'),
(61, 'cjgraham@dmacc.edu', '$2y$10$IAXb0G4dQeoovEF6ZFaLaOmggi7hTf3BOPQRQ7foI1N4rYItCsrha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'cmmiller26@dmacc.edu', '$2y$10$mLAZKBli52YjhlhJqOA4b.ByyrC2NhkzaxURy0zxgLq493DddCiTG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'achristensen5@dmacc.edu', '$2y$10$eqU5Gsfh5mNPXrsWCMJ.Xe02uDAKNC9Y91dvKYPurYm/xxcIegN9W', 'Ashland', 'Christensen', 'photography', 'https://ashlandchristensen.wixsite.com/photography', 'https://www.linkedin.com/in/ashlandchristensen2000', NULL, 'Windsor Heights', 'Being a photographer is such a dream of mine. I would love to be able to create memories and make clients happy with the results.', 'My hobbies are enjoying the little things in life. I like to go on adventures, make happy memories and do good things for other people.  ', 'IA', NULL, 'ashlandchristensen@gmail.com'),
(64, 'kmoon1@dmacc.edu', '$2y$10$8hqLJ68MOgm1joF4JyT/Me0WeAeUOoHFujmJRivDte8zie3pajR/m', 'kyle', 'moon', 'web-dev', 'http://kmoonpage.com/PortfolioDayPersonal.html', 'https://www.linkedin.com/in/kyle-moon-8795421a3/', 'http://kmoonpage.com/PortfolioDayPersonal.html', 'Ankeny', NULL, NULL, 'IA', 'Web site Design', 'redesignedMoon@gmail.com'),
(66, 'hvpurdy@dmacc.edu', '$2y$10$Fz32zfVq0vbRdBCX8U7tauiLtJSCVOqaaB2kVuxQmtlKaeFMbFTP6', 'Jane', 'Smith', 'graphic-design', NULL, NULL, NULL, NULL, NULL, NULL, 'IA', 'stuff', NULL),
(67, 'mlblair@dmacc.edu', '$2y$10$gba2AUnJjc8f1KNMPTufj.MbeS/PYwvpdhAAHJKTp/9er83.VUCrm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'gawebster1@dmacc.edu', '$2y$10$8fMAfNCb4UgpFFqt5Jvk2.XEXb8zqb9dE9duuOVqtzXTYJgIEJsNe', 'Grant', 'Webster', 'photography', 'https://www.grantwebsterphoto.com', 'https://www.linkedin.com/in/grantwebsterphoto', NULL, 'Indianola', 'Full-time Photographer', 'Hiking, exploring new places, and of course taking photos. ', 'IA', NULL, 'grantwebsterphoto@gmail.com'),
(69, 'ajlingle@dmacc.edu', '$2y$10$.6OCOuo1KDV0S8jAp9nObeY7nW7VRBRx7QUqo8ae3u8SwoZhXgGC6', 'Abagale', 'Lingle', 'photography', 'https://josephinephotography.squarespace.com/', 'https://www.linkedin.com/in/abagale-lingle-7519931a6/', NULL, 'Aspinwall', 'Photography has always been a big hobby of mine until one day I deiced to make a small business from my skills. Josephine Photography is a small said businesses of mine while I work towards my career in education. ', 'A few of my hobbies beside taking photos are showing cattle, hiking, kayaking, and hanging with friends.', 'IA', NULL, 'abbylingle16@gmail.com'),
(70, 'DSHartje@dmacc.edu', '$2y$10$urgsKW/LObSvu3hsKu5Gt.cxjFjzKFhKq6BORemwzroAH0fHyLFca', 'Dwight', 'Hartje', 'graphic-design', 'http://dwighthartje.us/', NULL, NULL, 'Ankeny', NULL, 'I have always had a large number of hobbies ranging from aviation and trains to photography and college football. Right now I am working on building a model airport of Raleigh-Durham KRDU Terminal 2. ', 'IA', 'Branding, Illustration', 'DSHartje89@gmail.com'),
(72, 'jrdowns2@dmacc.edu', '$2y$10$iya9Hr9t8zkNhxioaLd3ZutlWA525ikLVUf1UjewjMH6QZgwMOo.6', 'Jeremy', 'Downs', 'video-production', 'https://www.jeremydowns.com/', 'https://www.linkedin.com/in/jeremydownsart/', 'https://www.youtube.com/channel/UCWXFDMnnBphU4AQKa-9lktA', 'Johnston', 'I hope to join a creative team where I can contribute my video production and editing skills while applying my 15 years of client-facing design experience. Iâ€™m passionate about telling engaging stories. Please reach out if you think I\'d be a good fit!', 'With a lot of hobbies, there\'s a good chance I\'ll be somehow involved daily in one or more of the following: Photography, Cooking, Music, Movies, Treasure Hunting, Comic Book Collecting. I also love traveling and exploring new places.', 'IA', NULL, 'jeremydownsart@gmail.com'),
(73, 'mallen12@dmacc.edu', '$2y$10$3U2wwCySohJVBp0bIWMhqukudH2M3nUuNmhbKIDSe78e.g5tLqUmG', 'Morgan ', 'Allen', 'graphic-design', 'https://www.morganallen.work', 'https://www.linkedin.com/in/morgan-allen-175848174/', NULL, 'Newton', 'Become an expert in design and constantly learn new skills. ', NULL, 'IA', NULL, 'morganallen090@gmail.com'),
(76, 'jcwhaley@dmacc.edu', '$2y$10$mWGI6ayXeqdyoa1lff7uB.ceo6Obb3YyJ5q2hadC8NjAzls6zPYXq', 'Jessica ', 'Whaley ', 'photography', 'https://jcwhaley18.wixsite.com/jcphotos', 'https://www.linkedin.com/in/jessica-whaley-a56b2b1a2/', NULL, 'Mount Pleasant ', 'Become a tour photographer for some big music artist and travel across the globe with them then have my own studio ', 'Photography\r\nPainting\r\nCooking', 'IA', NULL, 'jcwhaley18@gmail.com'),
(77, 'tapplegate1@dmacc.edu', '$2y$10$amXD.15jZiiuDeEvjiz.5eb9CqfBFI9oyPZENfqMtjS5GEIy9PVEu', 'Tennessee', 'Applegate', 'photography', 'https://tennapplegatephoto.myportfolio.com', 'https://www.linkedin.com/in/tennessee-applegate-83a4301a6/', NULL, 'Johnston', 'I currently run a portrait business in Des Moines. I shoot seniors, families, engagements and weddings. Although, I have a passion for missions, and I am working towards becoming an international missions photographer.', 'Some of my favorite things are volunteering, traveling and food. I love to experience new cultures, places and people. The best things in life aren\'t actually things but living a life of adventure, purpose and growth.', 'IA', NULL, 'tennapplegate@yahoo.com'),
(78, 'awalker16@dmacc.edu', '$2y$10$ghyGs5sgBGnyjF1AMs.PwuUhalDeB/G7GvJkcRjE9EtUCB7bZg/y2', 'Andrea', 'Walker', 'graphic-design', 'https://www.artbyandreawalker.com/', 'https://www.linkedin.com/in/andrea-walker-68437818b', NULL, 'Des Moines', 'To work full time for a graphic design firm locally for a while. Then in the way future, once I get a feel for the industry and gain plenty of connections, I wish to build my own brand and business. ', 'I love to paint, whether it be landscapes, people, or abstracts, I try to paint when I\'m not designing. I\'m also a barista and a *huge* lover of coffee. If I\'m not making coffee or painting, then I\'m reading a book, probably outside, if it\'s nice out. ', 'IA', NULL, 'artbyandreawalker@gmail.com'),
(79, 'jdbusch2@dmacc.edu', '$2y$10$JTfG4BeKP00mCbP3KYA02ON0.x4xqJJRsiUfyPWLQxveHzrTBdW.2', 'Joseph', 'Busch', 'graphic-design', 'https://www.josephbusch.com/', 'https://www.linkedin.com/in/joseph-busch-b5a85b155/', NULL, 'Newton', 'Work for an agency or company focusing on UI/UX Design', 'Video games, Card games/tricks, and watching YouTube', 'IA', NULL, 'buschjoe99@gmail.com'),
(80, 'srpehl@dmacc.edu', '$2y$10$OTifLxV0BVdUoqPUhMTSzeNSLVO3lXj7a7xpWAORJ5rK6UrAXJbUK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 'ckargbo@dmacc.edu', '$2y$10$wDt0.7wf4E2lQr8QSwfXaucExlelLUvqR2Vkvn.watLWyhsAELgTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'phdavis@dmacc.edu', '$2y$10$QOc62U9aouBOlGGd4booNO4oxHbCJB1upc38N8Dy3ykpl3s4EW5vm', 'Parker', 'Davis', 'photography', 'https://www.phdavisphotovideo.com', 'https://www.linkedin.com/in/parker-davis-29841a1a7/', 'https://www.instagram.com/fra_m_e/', 'Ankeny', 'I would hope to go into commercial photography. I really enjoy shooting food, product, and lifestyle photography.', 'I\'m a huge coffee lover, I really enjoy watching movies, and I love traveling and learning about other cultures/languages. ', 'IA', NULL, 'phdavis.photovideo@gmail.com'),
(85, 'krjohnson7@dmacc.edu', '$2y$10$TQzuTQpSNkDjdG05QMUrZez.Ldh27Hp9SV38jO9w2DsoNi3e1mWg2', 'Kassondra', 'Johnson', 'graphic-design', 'https://www.kjohnsonart.com', 'https://www.linkedin.com/in/kass-johnson/', NULL, 'Des Moines', 'I am looking to broaden my experience as a graphic designer, specifically focusing on illustration.', 'Drawing: charcoal, graphite. Painting: oil, acrylic. Game Nights. Live music. ', 'IA', NULL, 'kassr3228@outlook.com'),
(86, 'ejohnson43@dmacc.edu', '$2y$10$5OsZn0C967U4ZYuNmjRv3O0znO/ZYz02JuWbtQvt3baIxkkIECwP.', 'Emily', 'Johnson', 'graphic-design', 'https://emj.we.bs/', 'https://www.linkedin.com/in/ahem-alee', NULL, 'Gladbrook', 'My immediate goal is to secure a position based on illustration and branding, where I can grow personally and professionally. I enjoy design challenges and look forward to opportunities with more responsibilities.', '-Repurposing Thrifted Furniture\r\n-Frisbee Golf\r\n-Reading\r\n-Longboarding\r\n-Illustration\r\n', 'IA', NULL, 'ahem.alee@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student_info_2020`
--
ALTER TABLE `student_info_2020`
  ADD PRIMARY KEY (`student_id`),
  ADD UNIQUE KEY `student_email_UNIQUE` (`student_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student_info_2020`
--
ALTER TABLE `student_info_2020`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
