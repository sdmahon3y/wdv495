// JavaScript Document

	var videoButton = document.getElementById("videoButton");
	var photoButton = document.getElementById("photoButton");
	var graphicsButton = document.getElementById("graphicDesignButton");
	var aniButton = document.getElementById("aniButton");
	
	aniButton.onmouseenter = function() {
		var aniHoverAnimation = anime({
		  targets: '#aniAnimation1, #aniAnimation2',
		  scale: .8,
		  easing: 'linear',
		  duration: 300,
		  translateX: 55,
		  translateY: 45,
		  loop: false
		})};
	
		aniButton.onmouseleave = function() {
		var photoHoverAnimation = anime({
		  targets: '#aniAnimation1, #aniAnimation2',
		  scale: 1,
		  translateX: 0,
		  translateY: 0,
		  easing: 'linear',
		  duration: 300,
		  loop: false
		});
			
		var photoHoverAnimation2 = anime({
		  targets: '#Lens2',
		  points: [
			{ value: ['204.85 288.44 239.26 348 310.72 348 345.36 288.22 309.76 228 240.11 228 204.85 288.44']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
 	};
	
	graphicsButton.onmouseenter = function() {
		var gdButtonAnimation = anime({
			targets: '#gDAnimation',
			rotate: '30',
			translateX: 100,
			translateY: -190,
			easing: 'easeOutBounce',
			duration: 600,
			loop: false
		});
	}
	
	graphicsButton.onmouseleave = function() {
		var gdButtonLeaveAnimation = anime({
		  targets: '#gDAnimation',
		  translateX: 0,
		  translateY: 0,
		  easing: 'linear',
		  rotate: '0',
		  duration: 600,
		  loop: false
		}); }
	
	photoButton.onmouseenter = function() {
		var photoHoverAnimation = anime({
		  targets: '#Lens',
		  points: [
			{ value: ['139.29 338.1 162.71 359.9 212.47 306.46 251.72 432.75 282.28 423.25 260.11 351.91 381.44 379.6 388.56 348.4 322.67 333.37 408.65 241.96 385.35 220.04 339.08 269.22 299.2 148 268.8 158 290.31 223.37 165.38 196.36 158.62 227.64 228.14 242.67 139.29 338.1']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
		
		var photoHoverAnimation2 = anime({
		  targets: '#Lens2',
		  points: [
			{ value: ['237.59 279.47 249.13 316.58 286.5 325.11 314.13 295.75 301.91 258.62 264.53 250.54 237.59 279.47']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
 	};
	
		photoButton.onmouseleave = function() {
		var photoHoverAnimation = anime({
		  targets: '#Lens',
		  points: [
			{ value: ['137.18 340.94 164.82 357.06 186.28 320.27 253.15 436 280.85 420 257.75 380 385 380 385 348 347.7 348 410.84 239.02 383.16 222.98 363.76 256.46 297.77 144.86 270.23 161.14 290.84 196 162 196 162 228 203.06 228 137.18 340.94']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
		
		var photoHoverAnimation2 = anime({
		  targets: '#Lens2',
		  points: [
			{ value: ['204.85 288.44 239.26 348 310.72 348 345.36 288.22 309.76 228 240.11 228 204.85 288.44']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
 	};
	
	// Video button animation
	videoButton.onmouseleave = function() { 
		anime.remove("#videoHover")
		var videoHoverAnimation = anime({
		  targets: '#videoHover',
		  points: [
			{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,147.5 206.6,147.5 206.6,180.4 168.5,180.4 168.5,127.6 151.7,127.6 151.7,180.4 113.6,180.4 113.6,147.5 96.8,147.5 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,230 113.6,230 113.6,197.1 151.7,197.1 151.7,249.9 168.5,249.9 168.5,197.1 206.6,197.1 206.6,230 223.3,230 223.3,197.1 261.6,197.1 261.6,250.2 ']
			}, ],
		  easing: 'easeInOutSine',
		  duration: 300,
		  loop: false
		});
 	};
	videoButton.onmouseenter = function() {
	
	var videoHoverAnimation = anime({
	  targets: '#videoHover',
	  points: [
		{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,147.5 206.6,147.5 206.6,180.4 168.5,180.4 168.5,127.6 151.7,127.6 151.7,180.4 113.6,180.4 113.6,147.5 96.8,147.5 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,230 113.6,230 113.6,197.1 151.7,197.1 151.7,249.9 168.5,249.9 168.5,197.1 206.6,197.1 206.6,230 223.3,230 223.3,197.1 261.6,197.1 261.6,250.2 ']
		},
		{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,127 206.6,127 206.6,180.4 168.5,180.4 168.5,113 151.7,113 151.7,180.4 113.6,180.4 113.6,147.5 96.8,147.5 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,230 113.6,230 113.6,197.1 151.7,197.1 151.7,263 168.5,263 168.5,197.1 206.6,197.1 206.6,250 223.3,250 223.3,197.1 261.6,197.1 261.6,250.2 ']
		},
		{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,139 206.6,139 206.6,180.4 168.5,180.4 168.5,98 151.7,98 151.7,180.4 113.6,180.4 113.6,127 96.8,127 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,250 113.6,250 113.6,197.1 151.7,197.1 151.7,278 168.5,278 168.5,197.1 206.6,197.1 206.6,238 223.3,238 223.3,197.1 261.6,197.1 261.6,250.2']
		}, 
		{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,147.5 206.6,147.5 206.6,180.4 168.5,180.4 168.5,127.6 151.7,127.6 151.7,180.4 113.6,180.4 113.6,139 96.8,139 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,236 113.6,236 113.6,197.1 151.7,197.1 151.7,249.9 168.5,249.9 168.5,197.1 206.6,197.1 206.6,230 223.3,230 223.3,197.1 261.6,197.1 261.6,250.2 ']
		},
		{ value: ['334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,147.5 206.6,147.5 206.6,180.4 168.5,180.4 168.5,127.6 151.7,127.6 151.7,180.4 113.6,180.4 113.6,147.5 96.8,147.5 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,230 113.6,230 113.6,197.1 151.7,197.1 151.7,249.9 168.5,249.9 168.5,197.1 206.6,197.1 206.6,230 223.3,230 223.3,197.1 261.6,197.1 261.6,250.2 ']
		},
	  ],
	  easing: 'linear',
	  duration: 900,
	  loop: true
	});	
	}