<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="description" content="DMACC Portfolio Day 2020">
  <meta name="keywords" content="DMACC, Portfolio Day, 2020, Des Moines Area Community College">
  <meta name="author" content="Bradley Owens">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="assets/favicon.png" sizes="32x32" type="image/png">
  <link rel="stylesheet" href="https://use.typekit.net/ygz0wqi.css">
  <title>Department Card</title>
  <link rel="stylesheet" href="departmentCard.css">
</head>
<body>
  <div class="container">
    <div class="test">
    </div>
    <div class="wrapper">
      <div class="department" id="web">
        <div class="dHeader">
          <h1>WEB DEVELOPMENT</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/webDevelopment.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="animation">
        <div class="dHeader">
          <h1>ANIMATION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/animation.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="graphic">
        <div class="dHeader">
          <h1>GRAPHIC DESIGN</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/graphicDesign.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="photography">
        <div class="dHeader">
          <h1>PHOTOGRAPHY</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/photography.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="departmentLast" id="video">
        <div class="dHeader">
          <h1>VIDEO PRODUCTION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/videoProduction.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
