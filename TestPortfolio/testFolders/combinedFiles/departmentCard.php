<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="description" content="DMACC Portfolio Day 2020">
  <meta name="keywords" content="DMACC, Portfolio Day, 2020, Des Moines Area Community College">
  <meta name="author" content="Bradley Owens">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="assets/favicon.png" sizes="32x32" type="image/png">
  <link rel="stylesheet" href="https://use.typekit.net/ygz0wqi.css">
  <title>Department Card</title>
  <link rel="stylesheet" href="departmentCard.css">
  <link href ="css/fonts.css" rel = "stylesheet" type = "text/css" />
  <link href ="css/styles.css" rel = "stylesheet" type = "text/css" />
</head>
<body>
  <div class="container">
    <div class="pHeader">
        <div class="center">
            <img class="icon" src="assets/animation_icon.png" alt="animation icon">
            <img class="icon" src="assets/design_icon.png" alt="design icon">
            <img class="icon" src="assets/photography_icon.png" alt="photography icon">
            <img class="icon" src="assets/video_icon.png" alt="video icon">
            <img class="icon" src="assets/web_icon.png" alt="web icon">
        </div>
        <hr class="lineOne"></hr>
        <hr class="lineTwo"></hr>
        <p class="logo"><img src="assets/Imaginati Logo.svg"></p>
        <hr class="lineTwo"></hr>
        <hr class="lineOne"></hr>
        <h2>DES MOINES AREA COMMUNITY COLLEGE</h2>
        <h1>PORTFOLIO DAY 2020</h1>
        <hr class="lineThree"></hr>
        <div class="triangle"></div>
        <p><img class="keys" src="assets/keys_desktop.png"></p>
    </div>
    <div class="wrapper">
      <div class="department" id="web">
        <div class="dHeader">
          <h1>WEB DEVELOPMENT</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/webDevelopment.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="animation">
        <div class="dHeader">
          <h1>ANIMATION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/animation.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="graphic">
        <div class="dHeader">
          <h1>GRAPHIC DESIGN</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/graphicDesign.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="department" id="photography">
        <div class="dHeader">
          <h1>PHOTOGRAPHY</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/photography.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
      <div class="departmentLast" id="video">
        <div class="dHeader">
          <h1>VIDEO PRODUCTION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/videoProduction.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
          <div class="student">
            <a href="#"><div class="studentImage">
              <img src="assets/student.jpg" id="studentImage" alt="Image representing a missing student." title="Student Image">
            </div>
            <div class="studentInfo">
              <h2>First Name</h2>
              <h2>Last Name</h2>
            </div></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
