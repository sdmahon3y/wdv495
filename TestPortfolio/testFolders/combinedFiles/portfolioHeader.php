<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Portfolio Website Header</title>
<link href ="css/fonts.css" rel = "stylesheet" type = "text/css" />
<link href ="css/styles.css" rel = "stylesheet" type = "text/css" />
</head>

<body>
<div>
<div class="center">
<img class="icon" src="images/animation_icon.png" alt="animation icon">
<img class="icon" src="images/design_icon.png" alt="design icon">
<img class="icon" src="images/photography_icon.png" alt="photography icon">
<img class="icon" src="images/video_icon.png" alt="video icon">
<img class="icon" src="images/web_icon.png" alt="web icon">
</div>
<hr class="lineOne"></hr>
<hr class="lineTwo"></hr>
<p class="logo"><img src="images/Imaginati Logo.svg"></p>
<hr class="lineTwo"></hr>
<hr class="lineOne"></hr>
<h2>DES MOINES AREA COMMUNITY COLLEGE</h2>
<h1>PORTFOLIO DAY 2020</h1>
<hr class="lineThree"></hr>
<div class="triangle"></div>
<p><img class="keys" src="images/keys_desktop.png"></p>
</div>
</body>
</html>