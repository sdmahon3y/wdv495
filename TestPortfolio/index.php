<?php

	require_once('dbConnector.php');

try {
    //create the sql command
	$sql = "SELECT student_first_name, student_last_name, student_id FROM student_info_2020 WHERE student_program='web-dev' ORDER BY student_last_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$webDev = $stmt->fetchAll();	//turn result set into an array.
    
    $sql = "SELECT student_first_name, student_last_name, student_id FROM student_info_2020 WHERE student_program='animation' ORDER BY student_last_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$animation = $stmt->fetchAll();	//turn result set into an array.
    
    $sql = "SELECT student_first_name, student_last_name,  student_id FROM student_info_2020 WHERE student_program='graphic-design' ORDER BY student_last_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$graphicDesign = $stmt->fetchAll();	//turn result set into an array.
    
    $sql = "SELECT student_first_name, student_last_name, student_id FROM student_info_2020 WHERE student_program='photography' ORDER BY student_last_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$photography = $stmt->fetchAll();	//turn result set into an array.
    
    $sql = "SELECT student_first_name, student_last_name, student_id FROM student_info_2020 WHERE student_program='video-production' ORDER BY student_last_name";

    //prepare the sql statement
	$stmt = $conn->prepare($sql);
	//bind the parameters if any
	//execute the statement
	$stmt->execute();
	//Work with the result-set from the SELECT command
	$videoProduction = $stmt->fetchAll();	//turn result set into an array.
    }

catch(PDOException $e) {
    echo "Process failed: " . $e->getMessage();
    }

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="description" content="DMACC Portfolio Day 2020">
  <meta name="keywords" content="DMACC, Portfolio Day, 2020, Des Moines Area Community College">
  <meta name="author" content="Bradley Owens">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="assets/favicon.png" sizes="32x32" type="image/png">
  <link rel="stylesheet" href="https://use.typekit.net/ygz0wqi.css">
  <title>Department Card</title>
  <link href="departmentCard.css" rel = "stylesheet" type = "text/css">
  <link href ="css/fonts.css" rel = "stylesheet" type = "text/css" />
  <link href ="css/styles.css" rel = "stylesheet" type = "text/css" />
	<link rel="stylesheet" type="text/css" href="pdayIcons.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.0/anime.min.js"></script>
	<link rel="stylesheet" href="https://use.typekit.net/ygz0wqi.css">
</head>
<body>
	       <div class="sticky">
	<nav>

	<div class="ButtonBox">
	 <!-- WebDev button -->
	<svg id="webDevIcon" x="0px" y="0px"
	 viewBox="0 0 40 40" xml:space="preserve">
		<rect id="webDevIconBackground" x="-0.3" y="-0.3" class="st0" width="40.8" height="40.6"/>
		<g>
		<polygon class="st1" id="webDevFlash" points="27.9,4.9 8.9,35.2 13.5,35.2 32.5,4.9 	"/>
		<polygon class="st1" points="20.5,7.4 4.8,7.4 4.8,23.2 7.8,23.2 7.8,10.5 20.5,10.5 	"/>
		<polygon class="st1" points="32.3,19.4 32.3,32.1 19.5,32.1 19.5,35.2 35.3,35.2 35.3,19.4 	"/>
		</g>
	</svg>
	<p>Web Development</p>
	</div>

	<div class="ButtonBox">
	<!-- Video button -->
	<svg version="1.1" id="videoButton" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 378 378" style="enable-background:new 0 0 378 378;" xml:space="preserve">
		<rect x="-0.3" class="st0" width="377.8" height="377.8"/>
			<polygon class="st1" id="videoHover" points="334.7,188.9 261.6,127.6 261.6,180.4 223.3,180.4 223.3,147.5 206.6,147.5 206.6,180.4 168.5,180.4 
				168.5,127.6 151.7,127.6 151.7,180.4 113.6,180.4 113.6,147.5 96.8,147.5 96.8,180.4 49.8,180.4 49.8,197.1 96.8,197.1 96.8,230 
				113.6,230 113.6,197.1 151.7,197.1 151.7,249.9 168.5,249.9 168.5,197.1 206.6,197.1 206.6,230 223.3,230 223.3,197.1 261.6,197.1 
				261.6,250.2 "/>
	</svg>
	<p>Video Production</p>
	</div>

	<!-- Photography button -->
	<div class="ButtonBox">
	<svg id="photoButton" data-name="Layer 1" viewBox="0 0 550 550">
	<rect class="cls-1" width="550" height="550"/>
	<g id="IconMiddleSpin">
	  <g>
		<polygon id="Lens" class="cls-2" points="137.18 340.94 164.82 357.06 186.28 320.27 253.15 436 280.85 420 257.75 380 385 380 385 348 347.7 348 410.84 239.02 383.16 222.98 363.76 256.46 297.77 144.86 270.23 161.14 290.84 196 162 196 162 228 203.06 228 137.18 340.94"/>
		<polygon  id="Lens2" class="cls-1" points="204.85 288.44 239.26 348 310.72 348 345.36 288.22 309.76 228 240.11 228 204.85 288.44"/>
	  </g>
	  <g>
		<polygon  class="cls-2" points="111.95 124.4 182.79 124.4 182.79 80.6 68.16 80.6 68.16 195.23 111.95 195.23 111.95 124.4"/>
		<polygon class="cls-2" points="111.96 380.69 68.16 380.69 68.16 495.32 182.79 495.32 182.79 451.52 111.96 451.52 111.96 380.69"/>
		<polygon class="cls-2" points="439.07 195.22 482.87 195.22 482.87 80.59 368.26 80.59 368.26 124.39 439.07 124.39 439.07 195.22"/>
		<polygon class="cls-2" points="439.08 380.69 439.08 451.51 368.26 451.51 368.26 495.31 482.88 495.31 482.88 380.69 439.08 380.69"/>
		<path class="cls-2" d="M276,165a123,123,0,1,1-87,36,122.16,122.16,0,0,1,87-36m0-32A155,155,0,1,0,431,288,155,155,0,0,0,276,133Z"/>
	  </g>
		<path class="cls-3" id="photoDot" d="M275.91,295.36c3.93,0,7.68-3.45,7.5-7.5a7.62,7.62,0,0,0-7.5-7.5c-3.92,0-7.68,3.45-7.5,7.5a7.63,7.63,0,0,0,7.5,7.5Z"/>
	</g>
	</svg>
	<p class="padButtonText">Photography</p>
	</div>


	<div class="ButtonBox">
	<!-- Graphic Design button -->
	<svg id="graphicDesignButton" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 450.3 451.05">
	  <rect class="cls-1" x="0.5" y="0.5" width="450.3" height="450.3"/>
	  <g id="gDAnimation"> 
		<rect class="cls-2" x="122.84" y="416.12" width="202.94" height="35.17"/>
		<path class="cls-2" d="M361.21,248.82l-4.52-5.6c-39-48.84-63.82-94-79.26-129.09-2.71-6-5.15-11.74-7.22-17.06C268,91.29,266,86,264.25,81,255.68,56.89,253.06,42.45,253,42.09l-1.45-8.76H198.17l-1.53,8.85c-.09.36-2.62,14.71-11,38.82-1.71,5-3.7,10.29-6,16.07-2.17,5.32-4.61,11-7.23,17.06-15.53,35-40.35,80.16-79.44,129.09l-4.51,5.6,59.76,103.72-24.83,52.09H326.18l-24.74-52.09ZM156.82,383.6l15.26-32.05L113.85,250.71c43.15-55.15,68.79-105,83.78-141.73,2.25-5.51,4.24-10.74,6.05-15.71,2-5.41,3.7-10.47,5.14-15.07,3.34-10.11,5.51-18.24,6.77-23.75h2.93V198.89h0c-47.21,3.79-83.59,48.56-85.22,50.55l-3.16,3.88,3.16,3.88C135,259.29,174.52,308,224.8,308s89.92-48.75,91.63-50.83l3.07-3.88-3.07-3.88c-1.62-2-37.92-46.76-85.22-50.55h0V54.45h2.93c1.35,5.51,3.43,13.64,6.77,23.75,1.45,4.6,3.16,9.66,5.15,15.07,1.8,5,3.79,10.2,6,15.71,15,36.74,40.62,86.58,83.68,141.73L277.61,351.55l15.26,32.05Zm68-167.19a37.13,37.13,0,0,1,34.85,24.37,37.54,37.54,0,0,1,0,25.1,37,37,0,1,1-34.85-49.47Zm-37.46,69c-19.41-10-34.49-25-40.9-32,6.5-7,21.49-22,40.9-32.14a49.17,49.17,0,0,0,0,64.19Zm75-64.1c19.41,10,34.48,25,40.89,32.05-6.41,7-21.39,22-40.89,32a49.08,49.08,0,0,0,0-64.1Z" transform="translate(0.5 1.29)"/>
		<path class="cls-2" d="M224.92,272.8a19.47,19.47,0,0,0,18.45-13.4l-15.34-6,15.15-6a19,19,0,0,0-18.26-13.59,19.52,19.52,0,1,0,0,39Z" transform="translate(0.5 1.29)"/>
	  </g>
	</svg>
	<p>Graphic<br>Design</p>
	</div>

	<div class="ButtonBox">
	<!-- Animation button -->
	<svg id="aniButton" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 449.8 449.8">
	  <rect class="cls-1" width="449.8" height="449.8"/>
	  <g>
		<polygon class="cls-2" id="aniAnimation1" points="124.99 251.15 125.31 251.34 125.31 251.5 125.58 251.5 218.99 306.15 225.55 294.94 232.16 306.18 325.38 251.29 325.82 251.29 325.82 142.79 314.34 142.79 320.13 132.89 226.13 77.89 225.96 78.19 225.64 77.64 131.64 132.64 137.7 143 125.31 143 125.31 250.61 124.99 251.15"/>
		<polygon class="cls-1" id="aniAnimation2" points="152.31 235.85 225.57 278.72 298.82 235.6 298.82 151.7 225.67 108.9 152.31 151.83 152.31 235.85"/>
	  </g>
	  <path class="cls-2" d="M76.88,104.55H103l-13,22.62Zm149.3,262.22-13.06-22.62h26.11Zm-14-65.14v30.09H191.6l34.58,59.9,34.57-59.9H239.34V201.2l59.74-35.14,27.17-16,24.41-14.36L360.07,152l34.58-59.9H325.48l11.58,20.05-25.27,14.87-26.92,15.84-59.12,34.77-59.11-34.77-26.93-15.84-26.39-15.53,11.2-19.39H55.35L89.94,152l9.79-17,25.53,15,27.17,16,59.73,35.14V301.63Zm161-197.08-13.06,22.62L347,104.55Z" transform="translate(0.3)"/>
	</svg>
	<p class="padButtonText">Animation</p>
	</div>

	</nav>	
<hr class="lineOne"></hr>
        <hr class="lineTwo"></hr>
</div>
  <div class="container">
    

<div>
	
<!-- Button Animations -->
<script src="buttonAnimations.js">
	
</script>
        
        <p class="logo"><img src="assets/Imaginati Logo.svg"></p>
        <hr class="lineTwo"></hr>
        <hr class="lineOne"></hr>
        <h2>DES MOINES AREA COMMUNITY COLLEGE</h2>
        <h1>PORTFOLIO DAY 2020</h1>
        <hr class="lineThree"></hr>
        <div class="triangle"></div>
        <p><img class="keys" src="assets/keys_desktop.png"></p>
    </div>
    <div class="wrapper">
      <div class="department" id="web">
        <div class="dHeader">
          <h1>WEB DEVELOPMENT</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/webDevelopment.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
              <?php
		          foreach ($webDev as $row) {
			         echo "<div class='student'><a href='student" . $row['student_id'] . "'><div class='studentImage'><img src='resizedImages/" . $row['student_first_name'] . " " . $row['student_last_name'] .  ".jpg' onerror=\"this.src='assets/student.jpg';\" id='studentImage' alt='Image for " . $row['student_first_name'] . " " . $row['student_last_name'] . "' title='Student Image'></div><div class='studentInfo'><h2>" . $row['student_first_name'] . "</h2><h2>" . $row['student_last_name'] . "</h2></div></a></div>";
		             }
	           ?>
        </div>
      </div>
      <div class="department" id="animation">
        <div class="dHeader">
          <h1>ANIMATION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/animation.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
              <?php
		          foreach ($animation as $row) {
			         echo "<div class='student'><a href='student" . $row['student_id'] . "'><div class='studentImage'><img src='resizedImages/" . $row['student_first_name'] . " " . $row['student_last_name'] .  ".jpg' onerror=\"this.src='assets/student.jpg';\" id='studentImage' alt='Image for " . $row['student_first_name'] . " " . $row['student_last_name'] . "' title='Student Image'></div><div class='studentInfo'><h2>" . $row['student_first_name'] . "</h2><h2>" . $row['student_last_name'] . "</h2></div></a></div>";
		             }
	           ?>
        </div>
      </div>
      <div class="department" id="graphic">
        <div class="dHeader">
          <h1>GRAPHIC DESIGN</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/graphicDesign.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
              <?php
		          foreach ($graphicDesign as $row) {
			         echo "<div class='student'><a href='student" . $row['student_id'] . "'><div class='studentImage'><img src='resizedImages/" . $row['student_first_name'] . " " . $row['student_last_name'] .  ".jpg' onerror=\"this.src='assets/student.jpg';\" id='studentImage' alt='Image for " . $row['student_first_name'] . " " . $row['student_last_name'] . "' title='Student Image'></div><div class='studentInfo'><h2>" . $row['student_first_name'] . "</h2><h2>" . $row['student_last_name'] . "</h2></div></a></div>";
		             }
	           ?>
        </div>
      </div>
      <div class="department" id="photography">
        <div class="dHeader">
          <h1>PHOTOGRAPHY</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/photography.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
              <?php
		          foreach ($photography as $row) {
			         echo "<div class='student'><a href='student" . $row['student_id'] . "'><div class='studentImage'><img src='resizedImages/" . $row['student_first_name'] . " " . $row['student_last_name'] .  ".jpg' onerror=\"this.src='assets/student.jpg';\" id='studentImage' alt='Image for " . $row['student_first_name'] . " " . $row['student_last_name'] . "' title='Student Image'></div><div class='studentInfo'><h2>" . $row['student_first_name'] . "</h2><h2>" . $row['student_last_name'] . "</h2></div></a></div>";
		             }
	           ?>
        </div>
      </div>
      <div class="departmentLast" id="video">
        <div class="dHeader">
          <h1>VIDEO PRODUCTION</h1>
          <div class="dLines">
          </div>
          <div class="dImage">
            <img src="assets/videoProduction.png" alt="Symbol representing school department." title="Department Image">
          </div>
        </div>
        <div class="sList">
              <?php
		          foreach ($videoProduction as $row) {
			         echo "<div class='student'><a href='student" . $row['student_id'] . "'><div class='studentImage'><img src='resizedImages/" . $row['student_first_name'] . " " . $row['student_last_name'] .  ".jpg' onerror=\"this.src='assets/student.jpg';\" id='studentImage' alt='Image for " . $row['student_first_name'] . " " . $row['student_last_name'] . "' title='Student Image'></div><div class='studentInfo'><h2>" . $row['student_first_name'] . "</h2><h2>" . $row['student_last_name'] . "</h2></div></a></div>";
		             }
	           ?>
        </div>
      </div>
    </div>
  </div>
  <footer>
	<br>
	<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>

	<div class="fb-like" data-href="https://www.facebook.com/dmaccportfolioday/" data-width="" data-layout="button_count" data-action="like" data-size="large" data-share="true"></div>

	<br><br>
	Designed by the Graphic Design Students & Developed by the Web Development Students.<br>
	Photos Provided by the Photography Students<br><br>
	Thursday May 7,	2020 Virtual Portfolio Day<br>
	© 2020 Des Moines Area Community College <br><br>
  </footer>
</body>
</html>
