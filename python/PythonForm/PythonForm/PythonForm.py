import os

print("Content-type: text/html\n")

qs = os.environ['QUERY_STRING']

if 'firstName' in qs:
    displayFirst = qs.split('=')[1]
else:
    displayFirst = 'First name not provided'

if 'lastName' in qs:
    displayLast = qs.split('=')[2]
else:
    displayLast = 'Last name not provided'

if 'school' in qs:
    displaySchool = qs.split('=')[3]
else:
    displaySchool = 'School not provided'

print ("<html>")
print ("<body>")
print ("<h1>First Name: " + displayFirst + "</h1>")
print ("<h1>Last Name: " + displayLast + "</h1>")
print ("<h1>School: " + displaySchool + "</h1>")
print ("</body>")
print ("</html>")
