import json

studentOne = {
    "student_major" : "Web Development",
    "student_id" : 332443,
    "student_gpa" : 3.6,
    "student_courses" : ["WDV101", "WDV131", "WDV105"]
    }
studentTwo = {
    "student_major" : "Web Development",
    "student_id" : 332443,
    "student_gpa" : 3.6,
    "student_courses" : ["WDV101", "WDV131", "WDV105"]
    }
studentThree = {
    "student_major" : "Web Development",
    "student_id" : 332443,
    "student_gpa" : 3.6,
    "student_courses" : ["WDV101", "WDV131", "WDV105"]
    }

students = { "array" : [studentOne, studentTwo, studentThree] }

s = json.dumps(students)
f = open("student.json", "w")
f.write(s)