// JavaScript Document

var student = {
	student_id: 332443,
	student_gpa: 3.6,
	student_courses: ["WDV101","WDV131","WDV105"]
}

declare function readyNow(student: { student_id:number, student_gpa:number, student_courses:string }): string;
{
    let studentID = document.getElementById('ID') as HTMLInputElement;
    studentID.value = student.student_id.toString(6);
    
    let studentGPA = document.getElementById('GPA') as HTMLInputElement;
    studentGPA.value = student.student_gpa.toString(3);
    
    for(var x:number = 0; student.student_courses.length; x++)
    {
        let course = student.student_courses[x]; //Creates variable for current state
        let list = "<li>" + course + "</li>";
        var courses = courses.concat(list);
    }
    
    let studentCourses = document.getElementById('COURSES') as HTMLInputElement;
    studentCourses.value = courses;
    
}